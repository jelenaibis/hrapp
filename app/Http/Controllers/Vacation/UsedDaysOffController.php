<?php

namespace App\Http\Controllers;

use App\Models\UsedDaysOff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsedDaysOffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'dayoff_date'=> 'required|string|max:255',
            'employee_id'=> 'required',
            'vacation_type_id'=> 'required',
            'employee_vacation_days_id'=> 'required',
        ]);


        if ($validator->fails()) {

            return response()->json([
                'status' => 400,
                'errors' => $validator->getMessageBag(),
                'error' => "Proverite da li ste uneli sve podatke ispravno!"

            ]);
        } else {
            
            $usedDayOFF = new UsedDaysOff();
            
            $usedDayOFF->dayoff_date = Carbon::parse($request->input('dayoff_date'));
            $usedDayOFF->employee_id = $request->input('employee_id');
            $usedDayOFF->vacation_type_id = $request->input('vacation_type_id');
            $usedDayOFF->employee_vacation_days_id = $request->input('employee_vacation_days_id');
            
            $usedDayOFF->save();
            
            return response()->json([
                'status' => 200,
                'message' => 'Dayoff saved!',
                

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UsedDaysOff  $usedDaysOff
     * @return \Illuminate\Http\Response
     */
    public function show(UsedDaysOff $usedDaysOff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UsedDaysOff  $usedDaysOff
     * @return \Illuminate\Http\Response
     */
    public function edit(UsedDaysOff $usedDaysOff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UsedDaysOff  $usedDaysOff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsedDaysOff $usedDaysOff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UsedDaysOff  $usedDaysOff
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsedDaysOff $usedDaysOff)
    {
        //
    }
}
