<?php

namespace App\Http\Controllers;

use App\Models\Vacation_Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class VacationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=> 'required|string|max:255',
            'default_days'=> 'required|numeric',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'status' => 400,
                'errors' => $validator->getMessageBag(),
                'errors' => "Proverite da li ste uneli sve podatke ispravno!"

            ]);
        } else {

            $vtype = new Vacation_Type();
            $vtype->name = $request->input('name');
            $vtype->max_days = $request->input('default_days');
            
            $vtype->save();
            
            return response()->json([
                'status' => 200,
                'message' => 'Uspesno ste kreirali tip odmora!',
                

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vacation_Type  $vacation_Type
     * @return \Illuminate\Http\Response
     */
    public function show(Vacation_Type $vacation_Type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vacation_Type  $vacation_Type
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacation_Type $vacation_Type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vacation_Type  $vacation_Type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacation_Type $vacation_Type)
    {
        $validator = Validator::make($request->all(), [
            'name'=> 'required|string|max:255',
            'default_days'=> 'required|numeric',
            'id'=>'required'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors());

        $vtype = Vacation_Type::firstOrFail($request->input('id'));

        $vtype = new Vacation_Type();
        $vtype->name = $request->input('name');
        $vtype->max_days = $request->input('default_days');
            
        $vtype->save();
            
            

        return response()->json(['Podaci o tipu odmora su izmenjeni.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vacation_Type  $vacation_Type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacation_Type $vacation_Type)
    {
        $vacation_Type->delete();

        return response()->json('Tip odmora je uspesno obrisan!');
    }
}
