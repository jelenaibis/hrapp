<?php

namespace App\Http\Controllers;

use App\Models\Employee_vacation_days;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class EmployeeVacationDaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'total_days_num'=> 'required|numeric',
            'employee_id'=> 'required',
            'vacation_type_id'=>'required'
        ]);



        if ($validator->fails()) {

            return response()->json([
                'status' => 400,
                'errors' => $validator->getMessageBag(),
                'error' => "Proverite da li ste uneli sve podatke ispravno!"

            ]);
        } else {
            
            $employeeVacDays = new Employee_vacation_days();
            $employeeVacDays->total_days_num = $request->input('total_days_num');
            $employeeVacDays->remaining_days_num = $request->input('total_days_num');
            $employeeVacDays->employee_id = $request->input('employee_id');
            $employeeVacDays->vacation_type_id = $request->input('vacation_type_id');
            
            $employeeVacDays->save();
            
            return response()->json([
                'status' => 200,
                'message' => 'Uspesno ste kreirali tip odmora!',
                

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee_vacation_days  $employeeVacDays_vacation_days
     * @return \Illuminate\Http\Response
     */
    public function show(Employee_vacation_days $employeeVacDays_vacation_days)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee_vacation_days  $employeeVacDays_vacation_days
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee_vacation_days $employeeVacDays_vacation_days)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee_vacation_days  $employeeVacDays_vacation_days
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        //videcemo sta moze da izmeni
        $validator = Validator::make($request->all(), [
            'total_days_num'=> 'required|numeric',
            'id'=>'required'
            
            
        ]);

        if ($validator->fails())
            return response()->json($validator->errors());

        $employeeVacDays = Employee_vacation_days::firstOrFail($request->input('id'));

        
        
        $employeeVacDays->total_days_num = $request->input('total_days_num');
        $employeeVacDays->remaining_days_num = $request->input('total_days_num');
        
        $employeeVacDays->save();
            
            

        return response()->json(['Podaci o odmoru zaposlenog su izmenjeni.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee_vacation_days  $employeeVacDays_vacation_days
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee_vacation_days $employeeVacDays_vacation_days)
    {
        $employeeVacDays_vacation_days->delete();

        return response()->json('Tip odmora je uspesno obrisan!');
    }
}
