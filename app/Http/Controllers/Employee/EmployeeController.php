<?php

namespace App\Http\Controllers;

use App\Models\Employee;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

use function PHPUnit\Framework\isNull;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return $employees;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $table->id();
        //     $table->string('first_name');
        //     $table->string('last_name');
        //     $table->date('date_of_employment')->nullable();
        //     $table->string('position')->default('avgEmployee');
        
        $validator = Validator::make($request->all(), [
            'first_name'=> 'required|string|max:255',
            'last_name'=> 'required|string|max:255',
            'date_of_employment' => 'required',
            'position'=> 'required|string|max:255',
        ]);


        if ($validator->fails()) {

            return response()->json([
                'status' => 400,
                'errors' => $validator->getMessageBag(),
                'errors' => "Proverite da li ste uneli sve podatk ispravno!"

            ]);
        } else {

            $employee = new Employee();
            $employee->first_name = $request->input('firstName');
            $employee->last_name = $request->input('lastName');

            $employee->date_of_employment = Carbon::parse($request->input('date_of_employment'));


            
            $employee->position = $request->input('position');
            $employee->save();
            
            return response()->json([
                'status' => 200,
                'message' => 'Uspesno ste kreirali zaposlenog!',
                

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($employee_id)
    {
        $employee = Employee::find($employee_id);
        if(is_null($employee)){
            return response()->json(
                'Podaci nisu pronadjeni', 404);

        }

        return response()->json($employee);
    }


    public function employeeDaysOff($employee_id)
    {
        $employee = Employee::find($employee_id)->first();
        $daysOff = $employee->useddaysoffs;

        return response()->json([
            'status' => 200,
            'employee'=>$employee,
            'daysOff' => $daysOff,
        ]);
    }

    public function employeeVacationTypes($employee_id)
    {
        $employee = Employee::find($employee_id)->first();
        
        //lasy loading days off
        $employee->vacation_days;

        return response()->json([
            'status' => 200,
            'employee'=>$employee,
            //'daysOff' => $vacationTypes,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'=> 'required|string|max:255',
            'last_name'=> 'required|string|max:255',
            'date_of_employment' => 'required',
            'position'=> 'required|string|max:255',
            'id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors());

        $employee = Employee::firstOrFail($request->input('id'));

        $employee->first_name = $request->input('firstName');
        $employee->last_name = $request->input('lastName');

        $employee->date_of_employment = Carbon::parse($request->input('date_of_employment'));


            
        $employee->position = $request->input('position');
        $employee->save();
            

        return response()->json(['Podaci o zaposlenom su izmenjeni.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return response()->json('Zaposleni je uspesno obrisan!');
    }
}
