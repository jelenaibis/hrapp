<?php

namespace App\Http\Controllers;

use App\Models\user_role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserRoleseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'=> 'required',
            'role_id'=> 'required',
            
        ]);

        if ($validator->fails()) {

            return response()->json([
                'status' => 400,
                'errors' => $validator->getMessageBag(),
                'errors' => "Proverite da li ste uneli sve podatke ispravno!"

            ]);
        } else {

            $user_role = new user_role();
            $user_role->user_id = $request->input('user_id');
            $user_role->role_id = $request->input('role_id');
            
            
            $user_role->save();
            
            return response()->json([
                'status' => 200,
                'message' => 'Uspesno ste dodelili useru ulogu!',
                

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user_role  $user_role
     * @return \Illuminate\Http\Response
     */
    public function show(user_role $user_role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user_role  $user_role
     * @return \Illuminate\Http\Response
     */
    public function edit(user_role $user_role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user_role  $user_role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user_role $user_role)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user_role  $user_role
     * @return \Illuminate\Http\Response
     */
    public function destroy(user_role $user_role)
    {
        //
    }
}
