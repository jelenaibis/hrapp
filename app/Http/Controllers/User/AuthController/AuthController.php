<?php

namespace App\Http\Controllers;

use App\Models\User;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


use ValidatorUser;

class AuthController extends Controller
{
   public function register(Request $request)
   {

        $validated = ValidatorUser::validateUserInput($request);

        if($validated){

            $user = User::create([
                'name' => $request->first_name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
    
            ]);
    
            $token = $user->createToken('auth_token')->plainTextToken;
    
            return response()->json([ 'data' => $user, 'access_token'=> $token, 'token_type'=>'Bearer']);

        }
        else{
            return response()->json(['message' => $validated]);

        }

        
    }

    public function login(Request $request){

        if(!Auth::atempt($request->only('email','password'))){
            return response()->json(['message'=>"Anauthorized"], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'access_token' => $token, 'token_type'=>'Bearer'
        ]);

    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return [
            'message' => 'Uspesnoste se odjavili!'
        ];
    }







}
