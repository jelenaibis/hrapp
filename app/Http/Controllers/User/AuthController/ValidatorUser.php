<?php


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ValidatorUser
{
    public static function validateUserInput(Request $request){

        $validator = Validator::make($request->all(), [
            'name'=> 'required|string|max:255',
            'email'=> 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:5'

        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }
        else{
            return true;
        }
    }
}
