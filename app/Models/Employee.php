<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employees';
    
    protected $fillable = [
        'first_name',
        'last_name',
        'date_of_employment', 
        'position'
    ];


    public function useddaysoffs()
    {
        return $this->hasMany(UsedDaysOff::class); //1 emoloyee ima vise
    }



    
    public function vacation_days() //dvidencij tipova odmora sa danima tipovi odmora
    {
        return $this->hasMany(Employee_vacation_days::class); //1 zapolseni ima vise tipova odmora
    }
}