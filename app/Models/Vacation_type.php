<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacation_type extends Model
{
    use HasFactory;
    protected $table = 'vacation_types';
    protected $guarded = ['id'];

    protected $fillable = [
        'date'
    ];


    public function useddaysoffs()
    {
        return $this->hasMany(UsedDaysOff::class); //1 tip ima vise iskoriscenih dana  odmora pitanje da li nam treba?
    }

    


    public function vacation_days()
    {
        return $this->hasMany(Employee_vacation_days::class); //1 tip ima vise dana odmora 
    }

}
