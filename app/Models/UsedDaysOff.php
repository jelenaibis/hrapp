<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsedDaysOff extends Model
{
    use HasFactory;
    protected $table = 'used_days_offs';
    protected $guarded = ['id'];

    protected $fillable = [
        'dayoff_date'
    ];


    public function employee()
    {
        return $this->belongsTo(Employee::class); //1 dan se odnosi samo na jednog zaposlenog
    }
    public function vacation_type()
    {
        return $this->belongsTo(Vacation_type::class); //1 dan ima samo 1 tip odmora
    }
    public function employee_vacation_days()
    {
        return $this->belongsTo(Employee_vacation_days::class); //1 dan ima samo 1 tip odmora
    }
    
}
