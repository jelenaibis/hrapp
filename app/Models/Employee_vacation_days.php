<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee_vacation_days extends Model
{
    use HasFactory;
    protected $table = 'employee_vacation_days';
    protected $guarded = ['id'];

    protected $fillable = [
        'total_days_num',
        'remaining_days_num'
    ];

    public function daysoffs()
    {
        return $this->hasMany(DaysOff::class); //1 odmor ima vise iskoiscenih dana odmora
    }


    public function employee()
    {
        return $this->belongsTo(Employee::class); //1 vacation_type_days se odnosi samo na 1 zaposlenog
    }
    public function vacaton_type()
    {
        return $this->belongsTo(Vacation_type::class); //1 vacation_type_days se odnosi samo na 1 vacationtype
    }
}