<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Employee;
use App\Models\Employee_vacation_days;
use App\Models\Vacation_type;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(3)->create();

        //  first_name');
        //  $table->string('last_name');
        //  $table->date('date_of_employment')->nullable();
        //  $table->string('position')->default('avgEmployee');
         $e1 = Employee::create([
            'first_name'=>'Jelena',
            'last_name' =>'Dikic',
            'date_of_employment' => '2022/11/28',
            'position'=> 'Junior software developer'
         ]);

         $e2 =Employee::create([
            'first_name'=>'Djordje',
            'last_name' =>'Djokic',
            'date_of_employment' => '2018/12/1',
            'position'=> 'Senior software developer'
         ]);
         $e3 = Employee::create([
            'first_name'=>'Ana',
            'last_name' =>'Vuceljic',
            'date_of_employment' => '2021/5/5',
            'position'=> 'Medior software developer'
         ]);

         $vt1 = Vacation_type::create([
            'name'=>'Sick leaves',
            'max_days'=>5
         ]);
         $vt2 = Vacation_type::create([
            'name'=>'Vacation',
            'max_days'=>30
         ]);
         $vt3 = Vacation_type::create([
            'name'=>'Blood donation',
            'max_days'=>2
         ]);

        

         $evt1 = Employee_vacation_days::create([
            'total_days_num'=> 2,
            'remaining_days_num'=>2,
            'employee_id'=> $e1->id,
            'vacation_type_id'=> $vt3->id
        ]);
        $evt2 = Employee_vacation_days::create([
            'total_days_num'=> 2,
            'remaining_days_num'=>2,
            'employee_id'=> $e2->id,
            'vacation_type_id'=> $vt3->id
        ]);
        $evt3 = Employee_vacation_days::create([
            'total_days_num'=> 2,
            'remaining_days_num'=>2,
            'employee_id'=> $e3->id,
            'vacation_type_id'=> $vt3->id
        ]);

        $evt11 = Employee_vacation_days::create([
            'total_days_num'=> 5,
            'remaining_days_num'=>5,
            'employee_id'=> $e1->id,
            'vacation_type_id'=> $vt1 ->id
        ]);
        $evt22 = Employee_vacation_days::create([
            'total_days_num'=> 5,
            'remaining_days_num'=>5,
            'employee_id'=> $e2->id,
            'vacation_type_id'=> $vt1 ->id
        ]);
        $evt32 = Employee_vacation_days::create([
            'total_days_num'=> 5,
            'remaining_days_num'=>5,
            'employee_id'=> $e3->id,
            'vacation_type_id'=> $vt1->id
        ]);
        $evt111 = Employee_vacation_days::create([
            'total_days_num'=> 22,
            'remaining_days_num'=>22,
            'employee_id'=> $e1->id,
            'vacation_type_id'=> $vt2 ->id
        ]);
        $evt222 = Employee_vacation_days::create([
            'total_days_num'=> 23,
            'remaining_days_num'=>23,
            'employee_id'=> $e2->id,
            'vacation_type_id'=> $vt2->id
        ]);
        $evt323 = Employee_vacation_days::create([
            'total_days_num'=> 30,
            'remaining_days_num'=>30,
            'employee_id'=> $e3->id,
            'vacation_type_id'=> $vt2->id
        ]);



        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
