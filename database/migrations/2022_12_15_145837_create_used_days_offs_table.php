<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('used_days_offs', function (Blueprint $table) {
            $table->id();
            $table->date('dayoff_date')->nullable();

            // $table->integer('employee_id');
            // $table->integer('vacation_type_id');
            
            // $table->integer('employee_vacation_days_id');

            $table->foreignId('employee_id')
                            ->references('id')
                            ->on('employees')
                            ->onDelete('cascade');
            $table->foreignId('vacation_type_id')
                            ->references('id')
                            ->on('vacation_types')
                            ->onDelete('cascade');
            $table->foreignId('employee_vacation_days_id')
                            ->references('id')
                            ->on('employee_vacation_days')
                            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('used_days_offs');
    }
};
