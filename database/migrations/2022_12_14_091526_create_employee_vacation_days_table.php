<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_vacation_days', function (Blueprint $table) {
            $table->id();

            // $table->integer('employee_id');
            // $table->integer('vacation_type_id');
            

            $table->integer('total_days_num')->default(0);
            $table->integer('remaining_days_num')->default(0);

            $table->timestamps();

            $table->foreignId('employee_id')->references('id')
            ->on('employees')->onDelete('cascade');

            $table->foreignId('vacation_type_id')->references('id')
            ->on('vacation_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_vacation_days');
    }
};
